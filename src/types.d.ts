import type { Browser, Tabs } from "webextension-polyfill";

declare global {
    const browser: Browser;
    const Tab: Tabs.Tab;
    const OnUpdatedChangeInfoType: Tabs.OnUpdatedChangeInfoType;
}

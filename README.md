• [Web site](https://omacronides.com/projets/firefox-pin-home-page)
• [Firefox addons](https://addons.mozilla.org/fr/firefox/addon/pinhomepage/)
• [Source](https://framagit.org/rui.nibau/pin-home-page)
• [Issues](https://framagit.org/rui.nibau/pin-home-page/-/issues)

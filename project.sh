# vars
VER=$npm_package_version
NAME=$npm_package_name

# dirs
DIR="$( cd "$( dirname "$0" )" && pwd )"
SRC_DIR=$DIR"/src/"
XPI_PATH=$DIR"/build/"$NAME".xpi"

build() {
    echo "Building extension..."
    if [[ -f $XPI_PATH ]]; then
        rm $XPI_PATH
    fi
    cd $SRC_DIR
    zip -r -FS $XPI_PATH * --exclude 'types.d.ts' --exclude 'icons/icon.xcf' --exclude 'web-ext-artifacts/*'
    echo "Done."
}

debug() {
    cd $SRC_DIR
    web-ext run --keep-profile-changes --firefox-profile=test
}

signUnlisted() {
    cd $SRC_DIR
    exho -e ""
    read -p "key:" key
    read -p "secret:" secret
    web-ext sign --channel=unlisted --api-key=$key --api-secret=$secret
}

case $1 in
    build) build;;
    debug) debug;;
esac

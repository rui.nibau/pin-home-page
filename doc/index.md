---
title:      Firefox : pin-home-page
date:       2023-08-07
cats:       [informatique]
tags:       [firefox, extensions]
techs:      [javascript, firefox]
intro:      A Really really simple addon for Firefox to pin home page
version:    1.6
source:     https://framagit.org/rui.nibau/pin-home-page
issues:     https://framagit.org/rui.nibau/pin-home-page/-/issues
download:   https://addons.mozilla.org/fr/firefox/addon/pinhomepage/
pagesAsSnippets: true
---

## Code

°°stx-js°°
    browser.browserSettings.homepageOverride.get({}).then(result => {
        /**@type {string[]} */
        const urls = (result.value || '').split('|');
        /**
         * @param {number} tabId 
         * @param {OnUpdatedChangeInfoType} changeInfo 
         * @param {Tab} tab 
         */
        const listener = (tabId, changeInfo, tab) => {
            if (changeInfo.status && changeInfo.status === 'complete') {
                const index = urls.indexOf(tab.url);
                if (index > -1) {
                    console.log(`Pin ${urls[index]}...`);
                    urls.splice(index, 1);
                    browser.tabs.update(tab.id, {pinned: true})
                    if (urls.length === 0) {
                        browser.tabs.onUpdated.removeListener(listener);
                    }
                }
            }
        };
        if (urls.length > 1 || (urls[0] && !urls[0].startsWith('chrome:') && !urls[0].startsWith('about:'))) {
            console.log(`Home page(s): [${urls}]`);
            browser.tabs.onUpdated.addListener(listener, {properties: ['status']});
        }
    }).catch(err => {
        console.error(err);
    });


## Licence

..include::./licence.md


## History

..include::./changelog.md


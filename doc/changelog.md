---
title:      Historique
date:       2023-08-07
updated:    2023-08-09
---

°°changelog°°
1.6 (2023-08-09):
    • fix: Pin at startup was not working.
    • upd: exclude home page and empty page
    • add: icon.
1.0 (2023-08-07):
    • add: initial release

